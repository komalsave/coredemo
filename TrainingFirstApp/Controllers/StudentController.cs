﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingFirstApp.Models;

namespace TrainingFirstApp.Controllers
{
    public class StudentController : Controller
    {
        Student st;
        // GET: Demo
        
        public ActionResult Index()
        {
            st = new Student();
            st.StudID = 1;
            st.Name = "Bob";
            st.Age = 20;
            st.Course = "B.Com";
            st.Email = "bob@gmail.com";
            return View(st);
        }
        public ActionResult Create(string id)
        {
            ViewBag.xyz = id;
            return View();
        }
        public ActionResult Edit()
        {
            return View("Index");
        }
        public ActionResult List()
        {
            List<Student> studList = new List<Student>();
            st = new Student();
            st.StudID = 1;
            st.Name = "Bob";
            st.Age = 20;
            st.Course = "B.Com";
            st.Email = "bob@gmail.com";
            studList.Add(st);
            st = new Student();
            st.StudID = 2;
            st.Name = "Joy";
            st.Age = 19;
            st.Course = "B.Sc.";
            st.Email = "joy@gmail.com";
            studList.Add(st);
            return View(studList);
        }
        
    }
}