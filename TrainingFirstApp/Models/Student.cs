﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingFirstApp.Models
{
    public class Student
    {
        public int StudID { get; set; }
        public string Name { get; set; }
        public string Course { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
    }
}