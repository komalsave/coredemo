﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingManagement.DAL;
using TrainingManagement.Models;
using System.Data;

namespace TrainingManagement.Controllers
{
    public class TraineeController : Controller
    {
        TraineeDAL tdal = new TraineeDAL();
        // GET: Trainee
        public ActionResult Index()
        {
            DataTable dt = tdal.GetTrainee();
            List<Models.Trainee> traineeList = new List<Models.Trainee>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Trainee trainee = new Trainee();
                trainee.TraineeId = Convert.ToInt32(dt.Rows[i]["TraineeId"]);
                trainee.Name = dt.Rows[i]["Name"].ToString();
                trainee.Technology = dt.Rows[i]["Technology"].ToString();
                trainee.Experience = Convert.ToInt32(dt.Rows[i]["Experience"]);
                trainee.ContactNo = dt.Rows[i]["ContactNo"].ToString();
                trainee.Email = dt.Rows[i]["EmailID"].ToString();
                traineeList.Add(trainee);
            }
            return View(traineeList);
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            Trainee trainee = new Trainee();
            DataTable dt = tdal.GetTrainee(id);
            trainee.TraineeId = Convert.ToInt32(dt.Rows[0]["TraineeId"]);
            trainee.Name = dt.Rows[0]["Name"].ToString();
            trainee.Technology = dt.Rows[0]["Technology"].ToString();
            trainee.Experience = Convert.ToInt32(dt.Rows[0]["Experience"]);
            trainee.ContactNo = dt.Rows[0]["ContactNo"].ToString();
            trainee.Email = dt.Rows[0]["EmailID"].ToString();

            return View("Edit", trainee);
        }

        [HttpPost]
        public ActionResult Create(Trainee trainee)
        {
            trainee.SelectType = 1;
            tdal.CRUD_Trainee(trainee);
            //return View("Index");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(Trainee trainee)
        {
            trainee.SelectType = 2;
            tdal.CRUD_Trainee(trainee);
            // return RedirectToAction("Index");
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            Trainee trainee = new Trainee();
            DataTable dt = tdal.GetTrainee(id);
            trainee.TraineeId = Convert.ToInt32(dt.Rows[0]["TraineeId"]);
            trainee.Name = dt.Rows[0]["Name"].ToString();
            trainee.Technology = dt.Rows[0]["Technology"].ToString();
            trainee.Experience = Convert.ToInt32(dt.Rows[0]["Experience"]);
            trainee.ContactNo = dt.Rows[0]["ContactNo"].ToString();
            trainee.Email = dt.Rows[0]["EmailID"].ToString();

            return View("Details", trainee);
        }

        [HttpGet]
        public ActionResult Delete(int id)
          {
            Trainee trainee = new Trainee();
            DataTable dt = tdal.GetTrainee(id);
            trainee.TraineeId = Convert.ToInt32(dt.Rows[0]["TraineeId"]);
            trainee.Name = dt.Rows[0]["Name"].ToString();
            trainee.Technology = dt.Rows[0]["Technology"].ToString();
            trainee.Experience = Convert.ToInt32(dt.Rows[0]["Experience"]);
            trainee.ContactNo = dt.Rows[0]["ContactNo"].ToString();
            trainee.Email = dt.Rows[0]["EmailID"].ToString();

            return View("Delete",trainee);
        }

        [HttpPost]
        public ActionResult Delete(int id,Trainee trainee)
        {
            //Trainee trainee=new Trainee();
            trainee.TraineeId = id;
            tdal.CRUD_Trainee(trainee);
            // return RedirectToAction("Index");
            return RedirectToAction("Index");
        }
    }
}