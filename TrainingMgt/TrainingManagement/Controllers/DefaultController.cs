﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingManagement.DAL;
using System.Data;
using TrainingManagement.Models;

namespace TrainingManagement.Controllers
{
    public class DefaultController : Controller
    {
        MenuMasterDAL menu = new MenuMasterDAL();
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MenuMaster()
        {
            //string JSONString = string.Empty;
            //JSONString = JsonConvert.SerializeObject(menu.getMenu());
            DataTable dt = menu.getMenu();
            List<Models.MenuMaster> MenuList = new List<Models.MenuMaster>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MenuMaster Menu = new MenuMaster();
                Menu.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                Menu.Name = dt.Rows[i]["Name"].ToString();
                Menu.ParentID = Convert.ToInt32(dt.Rows[i]["ParentID"]);
                Menu.Submenu = Convert.ToInt32(dt.Rows[i]["Submenu"]);
                MenuList.Add(Menu);
            }
            return View("getMenu", MenuList);

        }
    }
}