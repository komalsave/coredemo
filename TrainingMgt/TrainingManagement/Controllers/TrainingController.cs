﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingManagement.Models;
using TrainingManagement.DAL;
using System.Data;

namespace TrainingManagement.Controllers
{
    public class TrainingController : Controller
    {
        TrainingDAL tdal = new TrainingDAL();
        TrainerDAL trdal = new TrainerDAL();
        // GET: Training
        public ActionResult Index()
        {
            DataTable dt = tdal.GetTraining();
            List<Models.Training> trainingList = new List<Models.Training>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Training training = new Training();
                training.MasterID = Convert.ToInt32(dt.Rows[i]["MasterID"]);
                training.Training_Name = dt.Rows[i]["Training_Name"].ToString();
                training.Technology = dt.Rows[i]["Technology"].ToString();
                training.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
                training.place = dt.Rows[i]["place"].ToString();
                training.Seats = Convert.ToInt32(dt.Rows[i]["Seats"]);
                training.Start_Date = Convert.ToDateTime(dt.Rows[i]["Start_Date"]);
                training.End_Date = Convert.ToDateTime(dt.Rows[i]["End_Date"]);
                trainingList.Add(training);
            }
            return View("Index", trainingList);
        }
        [HttpPost]
        public ActionResult Create(Training training)
        {
            training.SelectType = 1;
            tdal.CRUD_TrainingMaster(training);
            return RedirectToAction("Index");
        }
        public ActionResult Create()
        {
            DataTable dt = trdal.GetTariner();
            Training t = new Training();
            
            List<Trainer> ddlTrainer = new List<Trainer>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Trainer tr = new Trainer();
                tr.TrainerId = Convert.ToInt32(dt.Rows[i]["TrainerId"]);
                tr.Name = dt.Rows[i]["Name"].ToString();

                ddlTrainer.Add(tr);

            }
            t.trainer = ddlTrainer;
            return View(t);
        }
       

//List<SelectListItem> ddlTrainer = new List<SelectListItem>() {
//        new SelectListItem {
//            Text = "ASP.NET MVC", Value = "1"
//        },
//        new SelectListItem {
//            Text = "ASP.NET WEB API", Value = "2"
//        },
//        new SelectListItem {
//            Text = "ENTITY FRAMEWORK", Value = "3"
//        },
//        new SelectListItem {
//            Text = "DOCUSIGN", Value = "4"
//        },
//        new SelectListItem {
//            Text = "ORCHARD CMS", Value = "5"
//        },
//        new SelectListItem {
//            Text = "JQUERY", Value = "6"
//        },
//        new SelectListItem {
//            Text = "ZENDESK", Value = "7"
//        },
//        new SelectListItem {
//            Text = "LINQ", Value = "8"
//        },
//        new SelectListItem {
//            Text = "C#", Value = "9"
//        },
//        new SelectListItem {
//            Text = "GOOGLE ANALYTICS", Value = "10"
//        },
//    };
//ViewBag.ddlTrainer = ddlTrainer;
            
    

 

        
        public ActionResult Edit()
        {
            DataTable dt = tdal.GetTraining();
            
                Training training = new Training();
                training.MasterID = Convert.ToInt32(dt.Rows[0]["MasterID"]);
                training.Training_Name = dt.Rows[0]["Training_Name"].ToString();
                training.Technology = dt.Rows[0]["Technology"].ToString();
                training.Days = Convert.ToInt32(dt.Rows[0]["Days"]);
                training.place = dt.Rows[0]["place"].ToString();
                training.Seats = Convert.ToInt32(dt.Rows[0]["Seats"]);
                training.Start_Date = Convert.ToDateTime(dt.Rows[0]["Start_Date"]);
                training.End_Date = Convert.ToDateTime(dt.Rows[0]["End_Date"]);

            return View("Edit", training);
        }


        [HttpPost]
        public ActionResult Edit(Training training)
        {
            training.SelectType = 2;
            tdal.CRUD_TrainingMaster(training);
            return RedirectToAction("Index");
        }

        public ActionResult Details()
        {
            DataTable dt = tdal.GetTraining();

            Training training = new Training();
            training.MasterID = Convert.ToInt32(dt.Rows[0]["MasterID"]);
            training.Training_Name = dt.Rows[0]["Training_Name"].ToString();
            training.Technology = dt.Rows[0]["Technology"].ToString();
            training.Days = Convert.ToInt32(dt.Rows[0]["Days"]);
            training.place = dt.Rows[0]["place"].ToString();
            training.Seats = Convert.ToInt32(dt.Rows[0]["Seats"]);
            training.Start_Date = Convert.ToDateTime(dt.Rows[0]["Start_Date"]);
            training.End_Date = Convert.ToDateTime(dt.Rows[0]["End_Date"]);
            return View("Details",training);
        }

        public ActionResult Delete(int id)
        {
            DataTable dt = tdal.GetTraining(id);

            Training training = new Training();
            training.MasterID = Convert.ToInt32(dt.Rows[0]["MasterID"]);
            training.Training_Name = dt.Rows[0]["Training_Name"].ToString();
            training.Technology = dt.Rows[0]["Technology"].ToString();
            training.Days = Convert.ToInt32(dt.Rows[0]["Days"]);
            training.place = dt.Rows[0]["place"].ToString();
            training.Seats = Convert.ToInt32(dt.Rows[0]["Seats"]);
            training.Start_Date = Convert.ToDateTime(dt.Rows[0]["Start_Date"]);
            training.End_Date = Convert.ToDateTime(dt.Rows[0]["End_Date"]);

            return View("Delete", training);
        }

        [HttpPost]
        public ActionResult Delete(int id,Training training)
        {
            training.MasterID = id;
            //trainer.SelectType = 2;
            tdal.CRUD_TrainingMaster(training);
            return RedirectToAction("Index");
        }

        public ActionResult Register()
        {
            DataTable dt = tdal.GetTraining();

            Training training = new Training();
            training.MasterID = Convert.ToInt32(dt.Rows[0]["MasterID"]);
            training.Training_Name = dt.Rows[0]["Training_Name"].ToString();
            training.Technology = dt.Rows[0]["Technology"].ToString();
            training.Days = Convert.ToInt32(dt.Rows[0]["Days"]);
            training.place = dt.Rows[0]["place"].ToString();
            training.Seats = Convert.ToInt32(dt.Rows[0]["Seats"]);
            training.Start_Date = Convert.ToDateTime(dt.Rows[0]["Start_Date"]);
            training.End_Date = Convert.ToDateTime(dt.Rows[0]["End_Date"]);
            return View("Register", training);
        }

        [HttpPost]
        public ActionResult Register(Training training)
        {
            training.SelectType = 1;
            tdal.CRUD_TrainingMaster(training);
            return RedirectToAction("Index");
        }

        //private static List<SelectListItem> GetTrainer()
        //{
        //    Trainer trainer= new Trainer();
        //    List<SelectListItem> customerList = (from p in TrainingMgmtEntities.Trainer.AsEnumerable()
        //                                         select new SelectListItem
        //                                         {
        //                                             Text = p.Name,
        //                                             Value = p.TrainerId.ToString()
        //                                         }).ToList();


        //    //Add Default Item at First Position.
        //    customerList.Insert(0, new SelectListItem { Text = "--Select Customer--", Value = "" });
        //    return customerList;
        //}
    }
}