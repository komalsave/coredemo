﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingManagement.DAL;
using TrainingManagement.Models;
using System.Data;

namespace TrainingManagement.Controllers
{
    public class TrainerController : Controller
    {
        TrainerDAL tdal = new TrainerDAL();
        // GET: Trainer
        public ActionResult Index()
        {
            DataTable dt = tdal.GetTariner();
            List<Models.Trainer> trainerList = new List<Models.Trainer>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Trainer trainer = new Trainer();
                trainer.TrainerId = Convert.ToInt32(dt.Rows[i]["TrainerId"]);
                trainer.Name = dt.Rows[i]["Name"].ToString();
                trainer.Technology = dt.Rows[i]["Technology"].ToString();
                trainer.Experience = Convert.ToInt32(dt.Rows[i]["Experience"]);
                trainer.ContactNo = dt.Rows[i]["ContactNo"].ToString();
                trainer.Email = dt.Rows[i]["EmailID"].ToString();
                trainerList.Add(trainer);
            }
                return View("Index",trainerList);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Trainer trainer)
        {
            trainer.SelectType = 1;
            tdal.CRUD_Tariner(trainer);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Trainer trainer = new Trainer();
            DataTable dt = tdal.GetTariner(id);
            trainer.TrainerId = Convert.ToInt32(dt.Rows[0]["TrainerId"]);
            trainer.Name = dt.Rows[0]["Name"].ToString();
            trainer.Technology = dt.Rows[0]["Technology"].ToString();
            trainer.Experience = Convert.ToInt32(dt.Rows[0]["Experience"]);
            trainer.ContactNo = dt.Rows[0]["ContactNo"].ToString();
            trainer.Email = dt.Rows[0]["EmailID"].ToString();

            return View("Edit",trainer);
        }

        
        [HttpPost]
        public ActionResult Edit(Trainer trainer)
        {
            trainer.SelectType = 2;
            tdal.CRUD_Tariner(trainer);
            return RedirectToAction("Index");
        }
        public ActionResult Details(int id)
        {
            Trainer Trainer = new Trainer();
            DataTable dt = tdal.GetTariner(id);
            Trainer.TrainerId = Convert.ToInt32(dt.Rows[0]["TrainerId"]);
            Trainer.Name = dt.Rows[0]["Name"].ToString();
            Trainer.Technology = dt.Rows[0]["Technology"].ToString();
            Trainer.Experience = Convert.ToInt32(dt.Rows[0]["Experience"]);
            Trainer.ContactNo = dt.Rows[0]["ContactNo"].ToString();
            Trainer.Email = dt.Rows[0]["EmailID"].ToString();

            return View("Details", Trainer);
        }

        public ActionResult Delete(int id)
        {
            Trainer Trainer = new Trainer();
            DataTable dt = tdal.GetTariner(id);
            Trainer.TrainerId = Convert.ToInt32(dt.Rows[0]["TrainerId"]);
            Trainer.Name = dt.Rows[0]["Name"].ToString();
            Trainer.Technology = dt.Rows[0]["Technology"].ToString();
            Trainer.Experience = Convert.ToInt32(dt.Rows[0]["Experience"]);
            Trainer.ContactNo = dt.Rows[0]["ContactNo"].ToString();
            Trainer.Email = dt.Rows[0]["EmailID"].ToString();

            return View("Delete", Trainer);
        }

        [HttpPost]
        public ActionResult Delete(Trainer trainer,int id)
        {
            //trainer.SelectType = 2;
            trainer.TrainerId = id;
            tdal.CRUD_Tariner(trainer);
            return RedirectToAction("Index");
        }
        
    }
}