﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using TrainingManagement.Models;

namespace TrainingManagement.DAL
{
    public class TrainerDAL
    {
        string connStr = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

        public DataTable CRUD_Tariner(Trainer trainer)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    using (SqlCommand cmd = new SqlCommand("Trainer_CRUD"))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.Add(new SqlParameter("@selectType", trainer.SelectType));
                        cmd.Parameters.Add(new SqlParameter("@TrainerID", trainer.TrainerId));
                        cmd.Parameters.Add(new SqlParameter("@Name", trainer.Name));
                        cmd.Parameters.Add(new SqlParameter("@Technology", trainer.Technology));
                        cmd.Parameters.Add(new SqlParameter("@Experience", trainer.Experience));
                        cmd.Parameters.Add(new SqlParameter("@ContactNo", trainer.ContactNo));
                        cmd.Parameters.Add(new SqlParameter("@EmailID", trainer.Email));
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return dt;
        }

        //get Data
        public DataTable GetTariner(int id=0)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    using (SqlCommand cmd = new SqlCommand("GetTrainer"))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.Add(new SqlParameter("@TrainerID", id));
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(dt);
                        }
                        con.Close();
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return dt;
        }
    }
}