﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TrainingManagement.Models;

namespace TrainingManagement.DAL
{
    public class TrainingDAL
    {
        string connStr = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

        public DataTable CRUD_TrainingMaster(Training training)
        {
            DataTable dt = new DataTable();
            try
            {

            
            using (SqlConnection con = new SqlConnection(connStr))
            {
                using (SqlCommand cmd = new SqlCommand("Training_Master_CRUD"))
                {
                    cmd.Connection = con;
                    cmd.Parameters.Add(new SqlParameter("@selectType", training.SelectType));
                    cmd.Parameters.Add(new SqlParameter("@MasterID", training.MasterID));
                    cmd.Parameters.Add(new SqlParameter("@Training_Name", training.Training_Name));
                    cmd.Parameters.Add(new SqlParameter("@Technology", training.Technology));
                    cmd.Parameters.Add(new SqlParameter("@Days", training.Days));
                    cmd.Parameters.Add(new SqlParameter("@place", training.place));
                    cmd.Parameters.Add(new SqlParameter("@Seats", training.Seats));
                    cmd.Parameters.Add(new SqlParameter("@Start_Date", "12/12/2020"));
                    cmd.Parameters.Add(new SqlParameter("@End_Date", "01/12/2022"));
                    cmd.Parameters.Add(new SqlParameter("@TrainerID", training.TrainerID));
                        
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            }
            catch (Exception)
            {

                throw;
            }
            return dt;
        }

        //get Data
        public DataTable GetTraining(int id = 0)
        {
            DataTable dt = new DataTable();
            try
            {
                
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    using (SqlCommand cmd = new SqlCommand("GetTraining"))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.Add(new SqlParameter("@MasterID", id));
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(dt);
                        }
                        con.Close();
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return dt;
        }

        public DataTable CRUD_TrainingDetails(Training training)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    using (SqlCommand cmd = new SqlCommand("Training_Details_CRUD"))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.Add(new SqlParameter("@selectType", training.SelectType));
                        cmd.Parameters.Add(new SqlParameter("@MasterID", training.MasterID));
                        cmd.Parameters.Add(new SqlParameter("@TraineeID", training.TraineeID));

                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return dt;
        }
    }
}