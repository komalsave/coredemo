﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using TrainingManagement.Models;

namespace TrainingManagement.DAL
{
    public class TraineeDAL
    {
        string connStr = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

        public DataTable CRUD_Trainee(Trainee trainee)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    using (SqlCommand cmd = new SqlCommand("Trainee_CRUD"))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.Add(new SqlParameter("@selectType", trainee.SelectType));
                        cmd.Parameters.Add(new SqlParameter("@TraineeID", trainee.TraineeId));
                        cmd.Parameters.Add(new SqlParameter("@Name", trainee.Name));
                        cmd.Parameters.Add(new SqlParameter("@Technology", trainee.Technology));
                        cmd.Parameters.Add(new SqlParameter("@Experience", trainee.Experience));
                        cmd.Parameters.Add(new SqlParameter("@ContactNo", trainee.ContactNo));
                        cmd.Parameters.Add(new SqlParameter("@EmailID", trainee.Email));
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return dt;
        }

        public DataTable GetTrainee(int id = 0)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    using (SqlCommand cmd = new SqlCommand("GetTrainee"))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.Add(new SqlParameter("@TraineeID", id));
                        cmd.CommandType = CommandType.StoredProcedure;
                        con.Open();
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(dt);
                        }
                        con.Close();
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return dt;
        }
    }
}