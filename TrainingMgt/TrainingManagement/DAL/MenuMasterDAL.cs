﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace TrainingManagement.DAL
{
    public class MenuMasterDAL
    {
        //SqlConnection con = new SqlConnection();
        string connStr = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

        //DataTable dt = new DataTable();
        public DataTable getMenu()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(connStr))
                {
                    using (SqlCommand cmd = new SqlCommand("GetMenu"))
                    {
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(dt);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                throw;
            }
            return dt;
        }
        
    }
}