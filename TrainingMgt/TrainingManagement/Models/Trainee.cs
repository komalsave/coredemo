﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrainingManagement.Models
{
    public class Trainee
    {
        public int SelectType { get; set; }

        public int TraineeId { get; set; }
        [Required(ErrorMessage ="Please Enter Name")]
        
        //[StringLength(200, ErrorMessage = "Name cannot be longer than 200 characters.")]
        public string Name { get; set; }
        
        public string Technology { get; set; }
      
        public int Experience { get; set; }
        //[Required(ErrorMessage = "Contact number is Required")]
        //[RegularExpression("[^0-9]", ErrorMessage = "Contact must be numeric")]
        //[MaxLength(10)]
        public string ContactNo { get; set; }
        //[EmailAddress(ErrorMessage ="Please Enter valid Email address")]
        [EmailAddress(ErrorMessage = "Please Enter valid Email address")]
        public string Email { get; set; }
        public int Deleted { get; set; }
    }
}