﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrainingManagement.Models
{
    public class Trainer
    {
        public int SelectType { get; set; }
        public int TrainerId { get; set; }
        [Required(ErrorMessage = "Please Enter Name")]
        public string Name{ get; set; }
        [Required(ErrorMessage = "Please Enter Technology")]
        public string Technology{ get; set; }
        [Required(ErrorMessage = "Please Enter Experience")]
        public int Experience{ get; set; }
        [MaxLength(ErrorMessage ="Please enter valid Contact No.")]
        public string ContactNo { get; set; }
        [EmailAddress(ErrorMessage = "Please Enter valid Email address")]
        public string Email{ get; set; }
        public int Deleted{ get; set; }


    }
}