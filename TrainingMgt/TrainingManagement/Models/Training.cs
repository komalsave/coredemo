﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingManagement.Models;

namespace TrainingManagement.Models
{
    public class Training
    {
        public int SelectType { get; set; }
        public int DetailsID { get; set; }
        public int MasterID { get; set; }
        public int TrainerID { get; set; }
        public int TraineeID { get; set; }
        public string Training_Name { get; set; }
        public string Technology { get; set; }
        public int Days { get; set; }
        public string place { get; set; }
        public int Seats { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime End_Date { get; set; }
        public List<Trainer> trainer { get; set; }
    }
}