﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingManagement.Models
{
    public class MenuMaster
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int ParentID { get; set; }
        public int Submenu { get; set; }
    }
}